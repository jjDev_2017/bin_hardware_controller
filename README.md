# NOTES
## this was created to run on Raspberry Pi

# SETUP
## Make sure i2c is enabled on your Raspberry Pi
    - sudo raspi-config
    - Go to Interfacing Options
    - Go to I2C and enable it

## Slight alterations are needed to the FaBo9axis_mpu9250 library for Python3
    - visit https://shiroku.net/robotics/how-to-run-fabo9axis_mpu9250-on-python3/

## SSH into Raspberry Pi as a headless machine and run the script
    - ssh -Y pi@<the-ip-address-of-rpi>
    - nohup python3 main.py