#!/usr/bin/env python
import asyncio
import websockets

##Magnetometer Stuff======
import FaBo9Axis_MPU9250
import time
import sys
##End Magnetometer-Related Stuff=====

##Servo-Related stuff==========
from time import sleep
import RPi.GPIO as GPIO
GPIO.setmode(GPIO.BCM)
GPIO.setwarnings(False)
##End Servo-Related stuff==========


class BinHardwareController:
    #if the difference in readings exceed this then it's classified as metal
    DIFFERENCE_TO_QUALIFY_AS_METAL = 20
    IP_ADDR = "192.168.0.147"
    PORT_NUM = 8765
    SERVO_PIN = 23 # Look for GPIO 23 on the RPi pin charts
    mpu9250 = FaBo9Axis_MPU9250.MPU9250()
    magnetometer_baseline = {"x": 0, "y": 0, "z": 0}


    def init(self):
        print("Listening in " + self.IP_ADDR +":"+ str(self.PORT_NUM))
        GPIO.setup(self.SERVO_PIN, GPIO.OUT)
        self.magnetometer_baseline = self.get_magnetometer_readings()
        print("---baseline---")
        print(self.magnetometer_baseline)
        print()

        self.initWebSockets()

    def initWebSockets(self):
        start_server = websockets.serve(self.listen_web_socket, self.IP_ADDR, self.PORT_NUM)
        asyncio.get_event_loop().run_until_complete(start_server)
        asyncio.get_event_loop().run_forever()

    async def listen_web_socket(self, websocket, path):
        while True:
            message_received = await websocket.recv()
            print(f"< {message_received}")

            response = f"Hello {message_received}!"

        

            if message_received == "run_servo":
                self.run_servo()
            elif message_received == "detect_metal":
                response = self.detect_metal(self.magnetometer_baseline)

            await websocket.send(response)
            print(f"> {response}")
        
    #returns the max value of each dimension x,y,z
    def get_magnetometer_readings(self):
        x_readings = []
        y_readings = []
        z_readings = []

        for i in range(0, 10):
            magReading = self.mpu9250.readMagnet()
            #print(magReading)
            # reading_total = {"x": magReading['x'], "y": magReading['y'], "z": magReading['z']}        
            x_readings.append(magReading['x'])
            y_readings.append(magReading['y'])
            z_readings.append(magReading['z'])
            time.sleep(0.1)

        result = {"x": max(x_readings), "y": max(y_readings), "z": max(z_readings)}
        return result
        
    def detect_metal(self, baseline_reading):
        result = "obj_not_metal"
        if self.object_is_metal(baseline_reading):
            result = "obj_is_metal"
        return result

    def object_is_metal(self, baseline_reading):
        result = False
        curReading = self.get_magnetometer_readings()
        
        cur_x_reading_has_increased = abs(curReading['x'] - baseline_reading['x']) >= self.DIFFERENCE_TO_QUALIFY_AS_METAL
        cur_y_reading_has_increased = abs(curReading['y'] - baseline_reading['y']) >= self.DIFFERENCE_TO_QUALIFY_AS_METAL
        cur_z_reading_has_increased = abs(curReading['z'] - baseline_reading['z']) >= self.DIFFERENCE_TO_QUALIFY_AS_METAL

        print(baseline_reading)
        print("<VS>")
        print(curReading)
        print()

        if cur_x_reading_has_increased or cur_y_reading_has_increased or cur_z_reading_has_increased:
            result = True
        return result


    def set_servo_angle(self, servo, angle):     
        pwm = GPIO.PWM(servo, 50)
        pwm.start(8)
        dutyCycle = angle / 18. + 3.
        pwm.ChangeDutyCycle(dutyCycle)
        sleep(0.3)
        pwm.stop()

    def run_servo(self):
        #OPEN HATCH
        self.set_servo_angle(self.SERVO_PIN, 50)
        sleep(1)

        #CLOSE HATCH
        self.set_servo_angle(self.SERVO_PIN, 180)


bin_controller = BinHardwareController()
bin_controller.init()